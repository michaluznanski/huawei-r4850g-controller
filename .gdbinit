# skip standard library files while stepping
skip -gfi /usr/include/c++/*/*/*
skip -gfi /usr/include/c++/*/*
skip -gfi /usr/include/c++/*

# connect with a gdb server
target remote localhost:3333
file ./build/kontroler-zasilacza.elf
# load
monitor reset halt
