import tkinter as tk
from dataclasses import asdict, astuple
from threading import Thread, Event
from time import sleep

from rectifier import Rectifier, Parameters

class App(tk.Tk):
    def __init__(self, uart_path:str, baud=115200, state_check_freq=.2, *args, **kwargs):
        super().__init__(*args, **kwargs)	
        self.title("Prostownik-app")
        self.rectifier = Rectifier(uart_path, baud)

        if not self.rectifier.check_alive():
            raise RuntimeError("Cannot communicate with rectifier")

        self.enable_frame = tk.Frame(self)
        self.enable_frame.pack(side="bottom", fill="both", expand=False)
        self.enable_button = tk.Button(self.enable_frame, text="Enable output", command=self._enable_output)
        self.enable_button.pack(side="left", fill="both", expand=True)
        self.disable_button = tk.Button(self.enable_frame, text="Disable output", command=self._disable_output)
        self.disable_button.pack(side="left", fill="both", expand=True)
        
        self.curr_params = Parameters()
        
        self.param_frame = tk.Frame(self, relief="sunken", borderwidth=3)
        self.param_frame.pack(side="left", fill="both", expand=True)
        
        self.state_subframe = tk.Frame(self.param_frame)
        self.state_subframe.pack(fill="both", expand=True)

        self.params_vals = []
        for idx, (field_name, field_val), in enumerate(asdict(self.curr_params).items()):
            tk.Label(self.state_subframe, text=field_name + ":").grid(column=0, row=idx, sticky="w")
            self.params_vals.append(tk.Label(self.state_subframe, text=str(field_val) ))
            self.params_vals[-1].grid(column=1, row=idx, sticky="e")
        self.state_subframe.columnconfigure((0, 1,), weight=1)
        self.state_subframe.rowconfigure(list(range(0, idx + 1)), weight=1)

        self.buttons_subframe = tk.Frame(self.param_frame)
        self.buttons_subframe.pack(fill="both", expand=False)

        self.start_measure_button = tk.Button(self.buttons_subframe, text="Start", command=self._start)
        self.start_measure_button.pack(fill="both", expand=True, side=tk.LEFT)
        self.stop_measure_button = tk.Button(self.buttons_subframe, text="Stop", command=self._stop)
        self.stop_measure_button.pack(fill="both", expand=True, side=tk.LEFT)
        
        self.control_frame = tk.Frame(self, relief="sunken", borderwidth=3)
        self.control_frame.pack(side="left", fill="both", expand=True)
        
        self.set_subframe = tk.Frame(self.control_frame)
        self.set_subframe.pack(side="bottom", fill="y", expand=False)
        self.set_voltage_button = tk.Button(self.set_subframe, text="Set voltage", command=self._set_voltage)
        self.set_voltage_button.pack(side="left", fill="both", expand=True)
        self.set_current_button = tk.Button(self.set_subframe, text="Set current", command=self._set_current)
        self.set_current_button.pack(side="left", fill="both", expand=True)

        self.nonvolatile_subframe = tk.Frame(self.control_frame)
        self.nonvolatile_subframe.pack(side="bottom", fill="y", expand=False)
        self.nonvolatile_label = tk.Label(self.nonvolatile_subframe, text="Nonvolatile mode")
        self.nonvolatile_label.pack(side="bottom", fill="both", expand=False)
        self.nonvolatile_var = tk.StringVar(value="off")
        self.nonvolatile_check_on = tk.Radiobutton(self.nonvolatile_subframe, text="on", variable=self.nonvolatile_var, value="on", command=self._nonvolatile_mode)
        self.nonvolatile_check_on.pack(side="left", fill="both", expand=True)
        self.nonvolatile_check_off = tk.Radiobutton(self.nonvolatile_subframe, text="off", variable=self.nonvolatile_var, value="off", command=self._nonvolatile_mode)
        self.nonvolatile_check_off.pack(side="left", fill="both", expand=True)
        
        self.voltage_slider = tk.Scale(self.control_frame, label="voltage", from_=42., to=58., orient="vertical",
            tickinterval=1, resolution=.5, showvalue=True, command=self._voltage_slide
        )
        self.voltage_slider.pack(side="left", fill="both", expand=True)

        self.current_slider = tk.Scale(self.control_frame, label="current", from_=0., to=60., orient="vertical",
            tickinterval=1, resolution=.1, showvalue=True, command=self._curr_slide
        )
        self.current_slider.pack(side="right", fill="both", expand=True)
        
        self._get_state_impl() # for side effects
        self.current_slider.set(self.curr_params.max_output_current)
        self.voltage_slider.set(self.curr_params.output_voltage)
        
        self.state_check_freq = state_check_freq
        self.get_state_event = Event()
        self.get_state_thread = Thread(target=self._get_state, daemon=True)
        self.get_state_thread.start()
        
    def _start(self):
        self.get_state_event.set()

    def _stop(self):
        self.get_state_event.clear()
    
    def _get_state_impl(self):
        if not (params := self.rectifier.get_state_raw()):
            print("Getting params error")
        else:
            self.curr_params = params
            for idx, value in enumerate(astuple(params)):
                self.params_vals[idx].config(text=f"{value:.2f}")
        
    def _get_state(self):
        while True:
            self.get_state_event.wait()
            self._get_state_impl()
            sleep(self.state_check_freq)

    def _nonvolatile_mode(self):
        print(self.nonvolatile_var.get())
        
    @staticmethod
    def _voltage_slide(val):
        print(val)
            
    @staticmethod
    def _curr_slide(val):
        print(val)

    def _set_voltage(self):
        self.rectifier.set_voltage(self.voltage_slider.get(), True if self.nonvolatile_var.get() == "on" else False)
        
    def _set_current(self):
        self.rectifier.set_current(self.current_slider.get(), True if self.nonvolatile_var.get() == "on" else False)
    
    def _enable_output(self):
        self.rectifier.enable()
    
    def _disable_output(self):
        self.rectifier.disable()
        
if __name__ == "__main__":
    app = App("/dev/ttyACM3")
    app.mainloop()
