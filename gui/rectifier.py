from enum import StrEnum, auto
from time import sleep
from dataclasses import dataclass
import struct
from threading import RLock

import serial

COMMANDS = [
        "check_alive",
        "echo",
        "get_state",
        "get_state_raw",
        "set_voltage",
        "set_current",
]

class Commands(StrEnum):
    CHECK_ALIVE = "check_alive"
    ECHO = "echo"
    ENABLE = "enable"
    DISABLE = "disable"
    GET_STATE = "get_state"
    GET_STATE_RAW = "get_state_raw"
    SET_VOLTAGE = "set_voltage"
    SET_CURRENT = "set_current"
    
@dataclass
class Parameters:
    input_voltage: float = 0.
    input_frequency: float = 0.
    input_current: float = 0.
    input_power: float = 0.
    input_temp: float = 0.
    efficiency: float = 0.
    output_voltage: float = 0.
    output_current: float = 0.
    output_current1: float = 0.
    max_output_current: float = 0.
    output_power: float = 0.
    output_temp: float = 0.
    amp_hour: float     = 0.
    
class Rectifier:
    RECT_MUTEX = RLock()
    
    def __init__(self, serial_path: str, baud: int = 115200):
        self.serial = serial.Serial(serial_path, baud)
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
        self.parameters_unpack_format = "<" + "f" * 13
        
    def __del__(self):
        self.serial.close()
        
    def _send_command(self, command: Commands, *args, **kwargs):
        with self.RECT_MUTEX:
            self.serial.reset_input_buffer()
            kwargs_mod = ["--" + key + "=" + val for key, val in kwargs.values()]
            serialized = " ".join([command, *args, *kwargs_mod]) + "\n"
            print(f"Sent command: \"{serialized[:-1]}\"")
            self.serial.write(serialized.encode("ascii"))
        
    def _send_and_read(self, command: Commands, *args, timeout=0.02, **kwargs) -> bytes:
        with self.RECT_MUTEX:
            self._send_command(command, *args, **kwargs)
            ret_bytes = bytes()
            sleep(timeout/2)
            while new_bytes := self.serial.read_all():
                ret_bytes += new_bytes
                sleep(timeout/2)
            print(f"Received answer: \"{ret_bytes}\"")
        return ret_bytes
        
    def check_alive(self):
        return self._send_and_read(Commands.CHECK_ALIVE).startswith(b"OK")
    
    def echo(self, strings: list[str]) -> bytes:
        return self._send_and_read(Commands.ECHO, *strings)

    def enable(self):
        self._send_command(Commands.ENABLE)

    def disable(self):
        self._send_command(Commands.DISABLE)
    
    def get_state(self) -> bytes:
        return self._send_and_read(Commands.GET_STATE, timeout=.05)
    
    def get_state_raw(self) -> Parameters | None:
        self.serial.reset_input_buffer()
        ans = self._send_and_read(Commands.GET_STATE_RAW, timeout=.05)
        try:
            return Parameters(*struct.unpack(self.parameters_unpack_format, ans))
        except struct.error:
            return None
        
    def set_voltage(self, voltage: float, nonvolatile=False, check_boundries=False) -> bool:
        if check_boundries:
            if voltage < 41. or voltage > 58.: 
                # nie potrzeba, jak mu nie pasuje to rzuca błędem
                return False
        if nonvolatile:
            ans = self._send_and_read(Commands.SET_VOLTAGE, str(voltage), "non-volatile")
        else:
            ans = self._send_and_read(Commands.SET_VOLTAGE, str(voltage))
        return b"Success" in ans

    def set_current(self, current: float, nonvolatile=False, check_boundries=False) -> bool:
        if check_boundries:
            if current < 0. or current > 60.: # TODO sprawdzić 
                # nie potrzeba, jak mu nie pasuje to rzuca błędem
                return False
        if nonvolatile:
            ans = self._send_and_read(Commands.SET_CURRENT, str(current), "non-volatile")
        else:
            ans = self._send_and_read(Commands.SET_CURRENT, str(current))
        return b"Success" in ans

if __name__ == "__main__":
    rectifier = Rectifier("/dev/ttyACM3")
    # print(rectifier.check_alive())
    # print(rectifier.echo(["dupa", "chuj", "", "asdf", " "]))
    print(rectifier.get_state())
    # print(rectifier.get_state_raw())

    print(rectifier.set_voltage(50.))
    print(rectifier.set_voltage(50., True))

    print(rectifier.set_current(50.))
    print(rectifier.set_current(5., True))
