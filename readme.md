# Huawei R4850G controller

A project of firmware and GUI to control a power supply - HuaweiR4850. It is made with use of STMG474REx MCU, although should be easily ported to any other STM32G4 device equipped with a FDCAN controller. The project's base was generate with the STM32CubeMX software and it is needed to get local copies of CMSIS and HAL drivers (they can be also manually copied).

By the firmware there is also made a simply GUI in Python to simplify usage of the driver.

To communicate between the MCU and the GUI running PC there is used USART2 in async mode.

Features:  
1. Set voltage
2. Set max current
3. Read and display current state
4. Enable and disable the device

The lattermost is implemented with a custom board with a transistor grounding a slot pin of the power supply. I'm too lazy to describe it here now, maybe in the future XD


Based on https://github.com/craigpeacock/Huawei_R4850G2_CAN
