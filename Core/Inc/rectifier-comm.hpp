#include <cstdint>
#include <atomic>
#include <string_view>
// zaczerpnięte z https://github.com/craigpeacock/Huawei_R4850G2_CAN
namespace RecitifierCom
{
namespace RectifierAddrs
{
constexpr uint32_t cRequestDataFrames = 0x10'80'40'FE;    // to request info
constexpr uint32_t cReceiveData = 0x10'81'40'7F;    // addr info comes from when reqtd
constexpr uint32_t cSetData    = 0x10'81'80'FE;
constexpr uint32_t cReceiveAck = 0x10'81'80'7E;

constexpr uint32_t cAhr = 0x10'01'11'7E;

// some others
constexpr uint32_t cAcknowledgment        = 0x10'81'40'7E;
constexpr uint32_t cDontKnow1             = 0x10'00'11'FE;
constexpr uint32_t cDontKnow2             = 0x10'80'81'FE;
constexpr uint32_t cOutputEnabledDisabled = 0x10'81'11'FE;
}    // namespace RectifierAddrs

#define MAX_CURRENT_MULTIPLIER 20.0f

#define R48xx_DATA_INPUT_POWER        0x70
#define R48xx_DATA_INPUT_FREQ         0x71
#define R48xx_DATA_INPUT_CURRENT      0x72
#define R48xx_DATA_OUTPUT_POWER       0x73
#define R48xx_DATA_EFFICIENCY         0x74
#define R48xx_DATA_OUTPUT_VOLTAGE     0x75
#define R48xx_DATA_OUTPUT_CURRENT_MAX 0x76
#define R48xx_DATA_INPUT_VOLTAGE      0x78
#define R48xx_DATA_OUTPUT_TEMPERATURE 0x7F
#define R48xx_DATA_INPUT_TEMPERATURE  0x80
#define R48xx_DATA_OUTPUT_CURRENT     0x81
#define R48xx_DATA_OUTPUT_CURRENT1    0x82

struct RectifierParameters
{
    float input_voltage;
    float input_frequency;
    float input_current;
    float input_power;
    float input_temp;
    float efficiency;
    float output_voltage;
    float output_current;
    float output_current1;
    float max_output_current;
    float output_power;
    float output_temp;
    float amp_hour;    // how much current we used
};

// set data ack:
enum Ack
{
    successful = 0x1,
    error      = 0x21    // mos likely value out of range
};

constexpr inline uint32_t cFullMask = (1U << 29U) - 1U;
inline std::atomic_bool wait_for_data{false};
inline std::atomic_bool wait_for_ack{false};
inline RectifierParameters curr_params{};
inline std::string_view curr_ack_string{};

void init_can_filters();

std::string_view format_parameters_string(struct RectifierParameters* rp);
std::string_view format_ack_string(uint8_t* data);

bool get_state();
bool set_voltage(float voltage, bool nonvolatile);
bool set_current(float current, bool nonvolatile);

}    // namespace RecitifierCom
     /* Rectifier communication - End */