#ifndef COMMAND_CONTROLLER_HPP__
#define COMMAND_CONTROLLER_HPP__

#include <string_view>
#include <vector>
#include <cstdint>

namespace CommandsController
{

namespace Error
{
enum
{
    strange_error,
    no_such_command,
    command_error,    // when command exists, is launched and returns error
    uart_error,
    can_overrun,
    can_strange_msg,
    can_controller_error,
    can_wait_timeout,
    parse_error,
};
void send_error(uint8_t error = Error::strange_error, std::string_view msg = std::string_view{});
}  // namespace Error

void start();
void react();
void send_ans(std::string_view to_send);
std::vector<std::string_view> args_tokenize(std::string_view buf);
}    // namespace CommandsController

#endif