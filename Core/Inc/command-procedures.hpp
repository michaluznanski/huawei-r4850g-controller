#include <string_view>
#include <utility>

#include "static_map.hpp"

#include "command-controller.hpp"

namespace CommandProcedures
{
using CommandProcedure = bool (*)(std::string_view args);

bool check_alive(std::string_view args);
bool echo(std::string_view args);
bool enable(std::string_view args);
bool disable(std::string_view args);
bool get_state(std::string_view args);
bool get_state_raw(std::string_view args);
bool set_voltage(std::string_view args);
bool set_current(std::string_view args);

constexpr inline static_map<std::string_view, CommandProcedure, 8>
    cCommandProcedureMap{{
        {"check_alive", check_alive},
        {"echo", echo},
		{"enable", enable},
		{"disable", disable},
		{"get_state", get_state},
		{"get_state_raw", get_state_raw},
		{"set_voltage", set_voltage},
		{"set_current", set_current}
    }};

}  // namespace CommandProcedures