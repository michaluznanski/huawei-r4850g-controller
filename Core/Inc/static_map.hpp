/* Define to prevent recursive inclusion ----------------------------------- */
#ifndef STATIC_MAP_HPP_INCLUDED_
#define STATIC_MAP_HPP_INCLUDED_

#include <cstdint>
#include <cassert>
#include <utility>
#include <array>

template<typename KeyT, typename ValT, std::size_t N>
class static_map
{
public:
    using NodeT = std::pair<KeyT, ValT>;

private:
    std::array<NodeT, N> m_vals;
    std::size_t m_cur_len{0};
    // TODO powinien być indeks wolnych miejsc i wsparcie usuwania elementów

    void error()
    {
    }

public:
    constexpr static_map() noexcept = default;

    consteval static_map(std::initializer_list<NodeT> l) noexcept
    {
        if (l.size() != N)
        {
            error();
        }
        std::copy(l.begin(), l.end(), m_vals.begin());
        m_cur_len = l.size();
    }

    constexpr ValT& operator[](const KeyT& key) noexcept
    {
        for (std::size_t i{0}; i < m_cur_len; ++i)
        {
            if (m_vals[i].first == key)
            {
                return m_vals[i].second;
            }
        }
        assert(m_cur_len < (N - 1));
        new (&m_vals[m_cur_len]) NodeT(key, ValT());
        return m_vals[m_cur_len++].second;
    }

    constexpr ValT& operator[](KeyT&& key) noexcept
    {
        for (std::size_t i{0}; i < m_cur_len; ++i)
        {
            if (m_vals[i].first == key)
            {
                return m_vals[i].second;
            }
        }
        assert(m_cur_len < (N - 1));
        new (&m_vals[m_cur_len]) NodeT(std::move(key), ValT());
        return m_vals[m_cur_len++].second;
    }

    constexpr NodeT* find(const KeyT& key)
    {
        std::size_t i{0};
        for (; i < m_cur_len; ++i)
        {
            if (m_vals[i].first == key)
            {
                break;
            }
        }
        if (i == m_cur_len)
        {
            return end();
        }
        return &m_vals[i];
    }

    constexpr const NodeT* find(const KeyT& key) const
    {
        std::size_t i{0};
        for (; i < m_cur_len; ++i)
        {
            if (m_vals[i].first == key)
            {
                break;
            }
        }
        if (i == m_cur_len)
        {
            return cend();
        }
        return &m_vals[i];
    }

    constexpr auto begin() noexcept
    {
        return m_vals.begin();
    }

    constexpr auto end() noexcept
    {
        return m_vals.begin() + m_cur_len;
    };

    constexpr auto cbegin() const noexcept
    {
        return m_vals.cbegin();
    }

    constexpr auto cend() const noexcept
    {
        return m_vals.cbegin() + m_cur_len;
    }

    [[nodiscard]] constexpr std::size_t size() const noexcept
    {
        return m_cur_len;
    }

    [[nodiscard]] constexpr std::size_t max_size() const noexcept
    {
        return N;
    }

    [[nodiscard]] constexpr bool empty() const noexcept
    {
        return m_cur_len == 0;
    }

    [[nodiscard]] constexpr bool full() const noexcept
    {
        return m_cur_len == N;
    }
};

#endif /* STATIC_MAP_HPP_INCLUDED_ */

/**** END OF FILE ****/
