#include <atomic>
#include <string_view>
#include <algorithm>
#include <optional>
#include <vector>
#include <array>
#include <bit>
#include <ranges>

#include <cstdio>
#include <cstring>
#include <cassert>

#include "main.h"
#include "usart.h"
#include "fdcan.h"

#include "command-controller.hpp"
#include "rectifier-comm.hpp"

using namespace std::literals;

// types

// static funcs
static void signal_start_initialization()
{
    for (int i = 0; i < 6; ++i)
    {
        HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
        HAL_Delay(500);
    }
}

static void signal_start_loop()
{
    for (int i = 0; i < 6; ++i)
    {
        HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
        HAL_Delay(100);
    }
}

void mainer()
{
    signal_start_initialization();
    RecitifierCom::init_can_filters();
    CommandsController::start();

    signal_start_loop();
    while (true)
    {
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        CommandsController::react();
    }
}
