#include <bit>
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdio>

#include "fdcan.h"

#include "rectifier-comm.hpp"
#include "command-controller.hpp"

using namespace std::string_view_literals;

namespace RecitifierCom
{

static std::array<char, 1000> s_print_params_buf;

void parse_Ahr(uint8_t* data, struct RectifierParameters *rp)
{
	uint16_t value = __builtin_bswap16(*reinterpret_cast<uint16_t *>(&data[6]));
	rp->amp_hour += ((static_cast<float>(value) / 20.0f) * 0.377f);
}

static bool parse_incoming_data(uint8_t* data, RectifierParameters* rp)
{
    auto ret = false;
    uint32_t value{0};
    if constexpr (std::endian::native == std::endian::little)
    {
        auto help = std::bit_cast<std::array<std::byte, 4>>(
            reinterpret_cast<uint32_t&>(data[4]));
        std::ranges::reverse(help);
        value = std::bit_cast<decltype(value)>(help);
    }
    else
    {
        value = std::bit_cast<decltype(value)>(
            reinterpret_cast<uint32_t&>(data[4]));
    }

    switch (data[1])
    {
        case R48xx_DATA_INPUT_POWER:
            rp->input_power = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_INPUT_FREQ:
            rp->input_frequency = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_INPUT_CURRENT:
            rp->input_current = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_OUTPUT_POWER:
            rp->output_power = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_EFFICIENCY:
            rp->efficiency = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_OUTPUT_VOLTAGE:
            rp->output_voltage = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_OUTPUT_CURRENT_MAX:
            rp->max_output_current =
                static_cast<float>(value) / MAX_CURRENT_MULTIPLIER;
            break;

        case R48xx_DATA_INPUT_VOLTAGE:
            rp->input_voltage = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_OUTPUT_TEMPERATURE:
            rp->output_temp = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_INPUT_TEMPERATURE:
            rp->input_temp = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_OUTPUT_CURRENT1:
            // printf("Output Current(1) %.02fA\r\n", value / 1024.0);
            // rp->output_current = value / 1024.0;
            rp->output_current1 = static_cast<float>(value) / 1024.0f;
            break;

        case R48xx_DATA_OUTPUT_CURRENT:
            rp->output_current = static_cast<float>(value) / 1024.0f;
            /* This is normally the last parameter received. Print */
            // r4850_print_parameters(rp);
            ret = true;
            break;
            
        case 0xe:
            // unknown frame
            break;

        default:
            CommandsController::Error::send_error(
                CommandsController::Error::parse_error,
                "Received unknown param from rectifier");
            break;
    }
    return ret;
}

std::string_view format_ack_string(uint8_t* data)
{
    bool error = data[0] & 0x20;
    uint32_t value{0};
    if constexpr (std::endian::native == std::endian::little)
    {
        auto help = std::bit_cast<std::array<std::byte, 4>>(
            reinterpret_cast<uint32_t&>(data[4]));
        std::ranges::reverse(help);
        value = std::bit_cast<decltype(value)>(help);
    }
    else
    {
        value = std::bit_cast<decltype(value)>(
            reinterpret_cast<uint32_t&>(data[4]));
    }

    auto curr_ptr = s_print_params_buf.begin();
    switch (data[1])
    {
        case 0x00:
            curr_ptr += snprintf(curr_ptr,
                                 s_print_params_buf.cend() - curr_ptr,
                                 "%s setting on-line voltage to %.02fV\n",
                                 error ? "Error" : "Success",
                                 value / 1024.0);
            break;
        case 0x01:
            curr_ptr += snprintf(
                curr_ptr,
                s_print_params_buf.cend() - curr_ptr,
                "%s setting non-volatile (off-line) voltage to %.02fV\n",
                error ? "Error" : "Success",
                value / 1024.0);
            break;
        case 0x02:
            curr_ptr +=
                snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "%s setting overvoltage protection to %.02fV\n",
                         error ? "Error" : "Success",
                         value / 1024.0);
            break;
        case 0x03:
            curr_ptr += snprintf(curr_ptr,
                                 s_print_params_buf.cend() - curr_ptr,
                                 "%s setting on-line current to %.02fA\n",
                                 error ? "Error" : "Success",
                                 (float)value / MAX_CURRENT_MULTIPLIER);
            break;
        case 0x04:
            curr_ptr += snprintf(
                curr_ptr,
                s_print_params_buf.cend() - curr_ptr,
                "%s setting non-volatile (off-line) current to %.02fA\n",
                error ? "Error" : "Success",
                (float)value / MAX_CURRENT_MULTIPLIER);
            break;
        default:
            curr_ptr += snprintf(curr_ptr,
                                 s_print_params_buf.cend() - curr_ptr,
                                 "%s setting unknown parameter (0x%02X)\n",
                                 error ? "Error" : "Success",
                                 data[1]);
            break;
    }
    return {s_print_params_buf.cbegin(), curr_ptr};
}

std::string_view format_parameters_string(struct RectifierParameters* rp)
{
    s_print_params_buf[0] = '\n';
    auto curr_ptr         = &s_print_params_buf[1];
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Input Voltage %.02fV @ %.02fHz\n",
                         rp->input_voltage,
                         rp->input_frequency);
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Input Current %.02fA\n",
                         rp->input_current);
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Input Power %.02fW\n",
                         rp->input_power);
    curr_ptr += snprintf(curr_ptr, s_print_params_buf.cend() - curr_ptr, "\n");
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Output Voltage %.02fV\n",
                         rp->output_voltage);
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Output Current %.02fA of %.02fA Max, %.03fAh\n",
                         rp->output_current,
                         rp->max_output_current,
                         rp->amp_hour / 3600);
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Output Power %.02fW\n",
                         rp->output_power);
    curr_ptr += snprintf(curr_ptr, s_print_params_buf.cend() - curr_ptr, "\n");
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Input Temperature %.01f DegC\n",
                         rp->input_temp);
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Output Temperature %.01f DegC\n",
                         rp->output_temp);
    curr_ptr += snprintf(curr_ptr,
                         s_print_params_buf.cend() - curr_ptr,
                         "Efficiency %.01f%%\n",
                         rp->efficiency * 100);
    return {s_print_params_buf.cbegin(), curr_ptr};
}

void init_can_filters()
{
    HAL_FDCAN_ConfigGlobalFilter(&hfdcan1,
                                 FDCAN_REJECT,
                                 FDCAN_REJECT,
                                 FDCAN_REJECT_REMOTE,
                                 FDCAN_REJECT_REMOTE);
    [[maybe_unused]] auto hal_ret =
        HAL_FDCAN_ConfigRxFifoOverwrite(&hfdcan1,
                                        FDCAN_RX_FIFO0,
                                        FDCAN_RX_FIFO_OVERWRITE);
    assert(hal_ret == HAL_OK);
    // filters' init
    FDCAN_FilterTypeDef filter_receive_data{FDCAN_EXTENDED_ID,
                                            static_cast<uint32_t>(0),
                                            FDCAN_FILTER_MASK,
                                            FDCAN_FILTER_TO_RXFIFO0,
                                            RectifierAddrs::cReceiveData,
                                            cFullMask};
    hal_ret = HAL_FDCAN_ConfigFilter(&hfdcan1, &filter_receive_data);
    assert(hal_ret == HAL_OK);
    FDCAN_FilterTypeDef filter_ack{FDCAN_EXTENDED_ID,
                                   static_cast<uint32_t>(1),
                                   FDCAN_FILTER_MASK,
                                   FDCAN_FILTER_TO_RXFIFO0,
                                   RectifierAddrs::cReceiveAck,
                                   cFullMask};
    hal_ret = HAL_FDCAN_ConfigFilter(&hfdcan1, &filter_ack);
    assert(hal_ret == HAL_OK);

    // accept everything for debug purpouses
    FDCAN_FilterTypeDef filter_debug{FDCAN_EXTENDED_ID,
                                     static_cast<uint32_t>(2),
                                     FDCAN_FILTER_MASK,
                                     FDCAN_FILTER_TO_RXFIFO0,
                                     0x0,
                                     0x0};
    hal_ret = HAL_FDCAN_ConfigFilter(&hfdcan1, &filter_debug);
    assert(hal_ret == HAL_OK);

    // interrupts' enable
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_RX_FIFO0,
                                   FDCAN_INTERRUPT_LINE0);
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_RX_FIFO1,
                                   FDCAN_INTERRUPT_LINE0);
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_BIT_LINE_ERROR,
                                   FDCAN_INTERRUPT_LINE1);
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_PROTOCOL_ERROR,
                                   FDCAN_INTERRUPT_LINE1);
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_TX_FIFO_ERROR,
                                   FDCAN_INTERRUPT_LINE1);
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_SMSG,
                                   FDCAN_INTERRUPT_LINE1);
    HAL_FDCAN_ConfigInterruptLines(&hfdcan1,
                                   FDCAN_IT_GROUP_MISC,
                                   FDCAN_INTERRUPT_LINE1);
    hfdcan1.Instance->ILE |= FDCAN_ILE_EINT0 | FDCAN_ILE_EINT1;
    hfdcan1.Instance->IR = (1U << 24U) - 1;
    hfdcan1.Instance->IE = FDCAN_IE_RF0NE | FDCAN_IE_RF0FE | FDCAN_IE_RF0LE |
                           FDCAN_IE_RF1NE | FDCAN_IE_RF1FE | FDCAN_IE_RF1LE |
                           FDCAN_IE_MRAFE | FDCAN_IE_TOOE | FDCAN_IE_ELOE |
                           FDCAN_IE_EPE |
                           FDCAN_IE_BOE;    // coś świruje to | FDCAN_IE_ARAE;
                                            // na necie coś wspominają, że jest
                                            // to problem z cortex-debugiem
                                            // że w cubie nie występuje
    hfdcan1.Instance->IE &= ~(FDCAN_IE_TFEE);

    HAL_FDCAN_Start(&hfdcan1);
}

bool get_state()
{
    FDCAN_TxHeaderTypeDef header{
        .Identifier  = RecitifierCom::RectifierAddrs::cRequestDataFrames,
        .IdType      = FDCAN_EXTENDED_ID,
        .TxFrameType = FDCAN_DATA_FRAME,
        .DataLength  = FDCAN_DLC_BYTES_8,
        .ErrorStateIndicator = FDCAN_ESI_ACTIVE,
        .BitRateSwitch       = FDCAN_BRS_OFF,
        .FDFormat            = FDCAN_CLASSIC_CAN,
        .TxEventFifoControl  = FDCAN_NO_TX_EVENTS,
        .MessageMarker       = 0};
    uint8_t data[8] = {0};
    auto ret        = HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &header, data);
    if (ret != HAL_OK)
    {
        CommandsController::Error::send_error(
            CommandsController::Error::can_controller_error,
            "Cannot send msg");
        return false;
    }
    return true;
}

bool set_voltage(float voltage, bool nonvolatile)
{
    auto value = static_cast<uint16_t>(voltage * 1024.0f);
    FDCAN_TxHeaderTypeDef header{.Identifier =
                                     RecitifierCom::RectifierAddrs::cSetData,
                                 .IdType              = FDCAN_EXTENDED_ID,
                                 .TxFrameType         = FDCAN_DATA_FRAME,
                                 .DataLength          = FDCAN_DLC_BYTES_8,
                                 .ErrorStateIndicator = FDCAN_ESI_ACTIVE,
                                 .BitRateSwitch       = FDCAN_BRS_OFF,
                                 .FDFormat            = FDCAN_CLASSIC_CAN,
                                 .TxEventFifoControl  = FDCAN_NO_TX_EVENTS,
                                 .MessageMarker       = 0};
    uint8_t data[8] = {
        [0] = 0x01,
        [1] = static_cast<uint8_t>(nonvolatile ? 0x01 : 0x00),
        [2] = 0x00,
        [3] = 0x00,
        [4] = 0x00,
        [5] = 0x00,
        [6] = static_cast<uint8_t>((value & 0xFF00) >> 8),
        [7] = static_cast<uint8_t>(value & 0xFF),
    };
    auto ret = HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &header, data);
    if (ret != HAL_OK)
    {
        CommandsController::Error::send_error(
            CommandsController::Error::can_controller_error,
            "Cannot send msg");
        return false;
    }
    return true;
}

bool set_current(float current, bool nonvolatile)
{
    auto value = static_cast<uint16_t>(current * MAX_CURRENT_MULTIPLIER);
    FDCAN_TxHeaderTypeDef header{.Identifier =
                                     RecitifierCom::RectifierAddrs::cSetData,
                                 .IdType              = FDCAN_EXTENDED_ID,
                                 .TxFrameType         = FDCAN_DATA_FRAME,
                                 .DataLength          = FDCAN_DLC_BYTES_8,
                                 .ErrorStateIndicator = FDCAN_ESI_ACTIVE,
                                 .BitRateSwitch       = FDCAN_BRS_OFF,
                                 .FDFormat            = FDCAN_CLASSIC_CAN,
                                 .TxEventFifoControl  = FDCAN_NO_TX_EVENTS,
                                 .MessageMarker       = 0};
    uint8_t data[8] = {
        [0] = 0x01,
        [1] = static_cast<uint8_t>(nonvolatile ? 0x04 : 0x03),
        [2] = 0x00,
        [3] = 0x00,
        [4] = 0x00,
        [5] = 0x00,
        [6] = static_cast<uint8_t>((value & 0xFF00) >> 8),
        [7] = static_cast<uint8_t>(value & 0xFF),
    };
    auto ret = HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &header, data);
    if (ret != HAL_OK)
    {
        CommandsController::Error::send_error(
            CommandsController::Error::can_controller_error,
            "Cannot send msg");
        return false;
    }
    return true;
}

extern "C" void FDCAN1_IT0_IRQHandler()
{
    constexpr unsigned cRxFifo0Mask = 0b111;
    constexpr unsigned cRxFifo1Mask = cRxFifo0Mask << 3;
    if (hfdcan1.Instance->IR & FDCAN_IR_RF0L)
    {
        hfdcan1.Instance->IR = FDCAN_IR_RF0L;
        CommandsController::Error::send_error(
            CommandsController::Error::can_overrun);
    }
    if (hfdcan1.Instance->IR & cRxFifo0Mask)
    {
        hfdcan1.Instance->IR = cRxFifo0Mask;
        while (HAL_FDCAN_GetRxFifoFillLevel(&hfdcan1, FDCAN_RX_FIFO0))
        {
            FDCAN_RxHeaderTypeDef header;
            uint8_t data_buf[64];
            HAL_FDCAN_GetRxMessage(&hfdcan1, FDCAN_RX_FIFO0, &header, data_buf);
            switch (header.Identifier)
            {
                case RectifierAddrs::cAhr:
                {
                    parse_Ahr(data_buf, &curr_params);
                }
                break;
                case RectifierAddrs::cAcknowledgment:
                {
                    // nothing here
                }
                break;
                case RectifierAddrs::cOutputEnabledDisabled:
                {
                    CommandsController::send_ans(data_buf[5] == 1 ? "Output enabled"sv : "Output disabled"sv);
                }
                break;
                case RectifierAddrs::cReceiveData:
                {
                    if (wait_for_data)
                    {
                        // save incoming data
                        auto end_of_data =
                            parse_incoming_data(data_buf, &curr_params);
                        if (end_of_data)
                        {
                            wait_for_data = false;
                        }
                    }
                    else
                    {
                        CommandsController::Error::send_error(
                            CommandsController::Error::can_strange_msg,
                            "Received data info when not waiting for");
                    }
                }
                break;
                case RectifierAddrs::cReceiveAck:
                {
                    if (!wait_for_ack)
                    {
                        CommandsController::Error::send_error(
                            CommandsController::Error::can_strange_msg,
                            "Rcvd unexpected ACK");
                    }
                    else
                    {
                        curr_ack_string = format_ack_string(data_buf);
                        wait_for_ack    = false;
                    }
                }
                break;
                case RectifierAddrs::cDontKnow1:
                {
                    // nothing here
                }
                break;
                default:
                {
                    std::array<char, 100> framebuf;
                    std::size_t count = std::snprintf(framebuf.begin(),
                                                      framebuf.size(),
                                                      "FIFO0, Id: 0x%08X",
                                                      header.Identifier);
                    CommandsController::Error::send_error(
                        CommandsController::Error::can_strange_msg,
                        {framebuf.cbegin(), count});
                }
            }
        }
    }

    // debug, potem nie będzie potrzebne, bo w zasadzie nie powinno się zdarzać
    if (hfdcan1.Instance->IR & cRxFifo1Mask)
    {
        hfdcan1.Instance->IR = cRxFifo1Mask;
        while (HAL_FDCAN_GetRxFifoFillLevel(&hfdcan1, FDCAN_RX_FIFO1))
        {
            FDCAN_RxHeaderTypeDef header;
            uint8_t data_buf[64];
            HAL_FDCAN_GetRxMessage(&hfdcan1, FDCAN_RX_FIFO1, &header, data_buf);
            std::array<char, 100> framebuf;
            std::size_t count = std::snprintf(framebuf.begin(),
                                              framebuf.size(),
                                              "FIFO1, Id: 0x%08X",
                                              header.Identifier);
            CommandsController::Error::send_error(
                CommandsController::Error::can_strange_msg,
                {framebuf.cbegin(), count});
        }
    }
}

extern "C" void FDCAN1_IT1_IRQHandler()
{
    CommandsController::Error::send_error(
        CommandsController::Error::can_controller_error,
        "CAN controller has an error");
}

}    // namespace RecitifierCom
