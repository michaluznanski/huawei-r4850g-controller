#include "command-controller.hpp"
#include "command-procedures.hpp"

#include "usart.h"

#include <vector>
#include <atomic>
#include <optional>

#include <cstdio>

namespace CommandsController
{
namespace Error
{
void send_error(uint8_t error, std::string_view msg)
{
	// TODO przerobić na nieblokujące
    char buffer[1000];
    auto count = 0;
    if (msg.size())
    {
        count = snprintf(buffer, sizeof(buffer), "Error: 0x%02x, Msg: \"%.*s\"\n", error, msg.size(), msg.cbegin());
    }
    else
    {
        count = snprintf(buffer, sizeof(buffer), "Error: 0x%02x\n", error);
    }

    while (HAL_UART_GetState(&hlpuart1) == HAL_UART_STATE_BUSY_TX)
    {
        if (hlpuart1.Instance->ISR & USART_ISR_TC)
        {
            // XD
            HAL_UART_IRQHandler(&hlpuart1);
        }
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    }
    HAL_UART_Transmit_DMA(&hlpuart1,
                          reinterpret_cast<const uint8_t*>(buffer),
                          count);
}

}    // namespace Error


namespace internal_vars
{
static char uart_rx_buffer[1000];
static std::atomic_bool new_command_ready{false};
static std::size_t curr_pos = 0;
}    // namespace internal_vars

namespace internal_funcs
{


static std::optional<std::pair<CommandProcedures::CommandProcedure, std::string_view>> parse()
{
    std::string_view buf{internal_vars::uart_rx_buffer, internal_vars::curr_pos - 1}; // -1 to get rid of \n
	// auto end_of_first_field = buf.find_first_of(' ');
    auto end_of_first_field = std::ranges::find(buf, ' ');
	auto command_procedure = CommandProcedures::cCommandProcedureMap.find(std::string_view{buf.cbegin(), end_of_first_field});
	if (command_procedure != CommandProcedures::cCommandProcedureMap.cend())
	{
		auto args_end = buf.cbegin() + internal_vars::curr_pos - 1;
		auto args_begin = std::min(end_of_first_field + 1, args_end);
							 
		return std::make_pair(
			command_procedure->second,
			std::string_view{args_begin, args_end});
	}
	return std::nullopt;
    // for (std::size_t i = 0; i < sizeof(Command::cCommandStrings) /
    //                                 sizeof(Command::cCommandStrings[0]);
    //      ++i)
    // {
    //     if (buf.starts_with(Command::cCommandStrings[i]))
    //     {
    //         return std::make_pair(
    //             static_cast<Command::CommandNr>(i),
    //             std::string_view(buf.cbegin() +
    //                                  Command::cCommandStrings[i].size() + 1,
    //                              buf.cbegin() + internal_vars::curr_pos - 1));
    //     }
    // }
    // return std::nullopt;
}

static void continue_rx()
{

    auto hal_status = HAL_UARTEx_ReceiveToIdle_DMA(
        &hlpuart1,
        reinterpret_cast<uint8_t*>(internal_vars::uart_rx_buffer) +
            internal_vars::curr_pos,
        sizeof(internal_vars::uart_rx_buffer) - internal_vars::curr_pos);
    if (hal_status != HAL_OK)
    {
        Error::send_error(Error::uart_error);
        Error_Handler();
    }
}
}    // namespace internal_funcs

void start()
{
    internal_vars::curr_pos          = 0;
    internal_vars::new_command_ready = false;
    auto hal_status                  = HAL_UARTEx_ReceiveToIdle_DMA(
        &hlpuart1,
        reinterpret_cast<uint8_t*>(internal_vars::uart_rx_buffer),
        sizeof(internal_vars::uart_rx_buffer));
    if (hal_status != HAL_OK)
    {
        Error::send_error(Error::uart_error);
        Error_Handler();
    }
}

void react()
{
    if (internal_vars::new_command_ready)
    {
        auto command = internal_funcs::parse();
        if (!command)
        {
            Error::send_error(Error::no_such_command);
            goto endif;
        }
        if (!command->first(command->second))
        {
            Error::send_error(Error::command_error);            
        }

    endif:
        start();    // wait for new command
    }
}

void send_ans(std::string_view to_send)
{
    // while (HAL_DMA_GetState(hlpuart1.hdmatx) == HAL_DMA_STATE_BUSY)
    // {
    //     HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    // }
    while (HAL_UART_GetState(&hlpuart1) == HAL_UART_STATE_BUSY_TX)
    {
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    }

    // wait last transmition ends
    HAL_UART_Transmit_DMA(&hlpuart1,
                          reinterpret_cast<const uint8_t*>(to_send.cbegin()),
                          to_send.size());
}

std::vector<std::string_view> args_tokenize(std::string_view buf)
{
    std::vector<std::string_view> ret_vec;
    auto pos               = buf.find_first_of(' ');
    while (pos != std::string_view::npos)
    {
        ret_vec.emplace_back(buf.cbegin(), pos);
        buf      = std::string_view{buf.cbegin() + pos + 1, buf.cend()};
        pos      = buf.find_first_of(' ');
    }
    ret_vec.emplace_back(buf.cbegin(), buf.cend());
    return ret_vec;
}


extern "C" void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef* huart,
                                           uint16_t Size)
{
    if (huart->Instance == LPUART1)
    {
        internal_vars::curr_pos += Size;
        if (Size >= sizeof(internal_vars::uart_rx_buffer))
        {
            Error_Handler();
        }
        if (internal_vars::uart_rx_buffer[internal_vars::curr_pos - 1] != '\n')
        {
            // if not lf - wait on
            internal_funcs::continue_rx();
            return;
        }
        internal_vars::new_command_ready = true;
    }
}

extern "C" void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart)
{
    Error_Handler();
}

}    // namespace CommandsController