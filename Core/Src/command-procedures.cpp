#include "command-procedures.hpp"
#include "rectifier-comm.hpp"

#include "fdcan.h"

#include <charconv>
#include <cstdio>

using namespace std::string_view_literals;

bool CommandProcedures::check_alive(std::string_view args)
{
    CommandsController::send_ans("OK\n");
    return true;
}

bool CommandProcedures::echo(std::string_view args)
{
    static char buffer[1000];
    auto curr_ptr    = &buffer[0];
    auto end_ptr     = &buffer[1000];
    auto parsed_args = CommandsController::args_tokenize(args);
    for (std::size_t i = 0; i < parsed_args.size(); ++i)
    {
        curr_ptr += std::snprintf(curr_ptr,
                                  end_ptr - curr_ptr,
                                  "Arg. %u: \"%.*s\"\n",
                                  i + 1,
                                  parsed_args[i].size(),
                                  parsed_args[i].cbegin());
    }
    CommandsController::send_ans(std::string_view{buffer, curr_ptr});
    return true;
}

bool CommandProcedures::enable(std::string_view args)
{
    HAL_GPIO_WritePin(Pin_EnableRectifier_GPIO_Port, Pin_EnableRectifier_Pin, GPIO_PIN_SET);
    return true;
}

bool CommandProcedures::disable(std::string_view args)
{
    HAL_GPIO_WritePin(Pin_EnableRectifier_GPIO_Port, Pin_EnableRectifier_Pin, GPIO_PIN_RESET);
    return true;
}

bool CommandProcedures::get_state(std::string_view args)
{
    if (!RecitifierCom::get_state())
    {
        CommandsController::send_ans("CAN send error\n");
        return false;
    }
    RecitifierCom::wait_for_data = true;

    auto end_time = HAL_GetTick() + 1000U;
    while (RecitifierCom::wait_for_data)
    {
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        if (HAL_GetTick() > end_time)
        {
            CommandsController::Error::send_error(
                CommandsController::Error::can_wait_timeout,
                "Timeout elapsed while waiting for data");
            RecitifierCom::wait_for_data = false;
            return false;
        }
    }
    auto params_str =
        RecitifierCom::format_parameters_string(&RecitifierCom::curr_params);
    CommandsController::send_ans(params_str);
    return true;
}

bool CommandProcedures::get_state_raw(std::string_view args)
{
    if (!RecitifierCom::get_state())
    {
        CommandsController::send_ans("CAN send error\n");
        return false;
    }
    RecitifierCom::wait_for_data = true;

    auto end_time = HAL_GetTick() + 1000U;
    while (RecitifierCom::wait_for_data)
    {
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        if (HAL_GetTick() > end_time)
        {
            CommandsController::Error::send_error(
                CommandsController::Error::can_wait_timeout,
                "Timeout elapsed while waiting for data");
            RecitifierCom::wait_for_data = false;
            return false;
        }
    }
    std::string_view raw_data{
        reinterpret_cast<const char*>(&RecitifierCom::curr_params),
        sizeof(RecitifierCom::RectifierParameters)};
    CommandsController::send_ans(raw_data);
    return true;
}

bool CommandProcedures::set_voltage(std::string_view args)
{
    auto fields = CommandsController::args_tokenize(args);
    if (fields.size() < 1)
    {
        CommandsController::send_ans("Must be at least 1 argument!\n");
        return false;
    }
    float voltage{0.f};
    auto result =
        std::from_chars(fields[0].cbegin(), fields[0].cend(), voltage);
    if (result.ec != std::errc{})
    {
        CommandsController::send_ans(
            "Wrong first argument - must be real number\n");
        return false;
    }
    auto nonvolatile_flag = std::ranges::find(fields, "non-volatile"sv);

    if (!RecitifierCom::set_voltage(voltage, nonvolatile_flag != fields.cend()))
    {
        CommandsController::Error::send_error(
            CommandsController::Error::can_controller_error,
            "Sending CAN frame failed");
        return false;
    }
    RecitifierCom::wait_for_ack = true;
    auto end_time               = HAL_GetTick() + 1000U;
    while (RecitifierCom::wait_for_ack)
    {
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        if (HAL_GetTick() > end_time)
        {
            CommandsController::Error::send_error(
                CommandsController::Error::can_wait_timeout,
                "Timeout elapsed while waiting for ACK");
            RecitifierCom::wait_for_ack = false;
            return false;
        }
    }
    CommandsController::send_ans("\nGot ack string\n");
    CommandsController::send_ans(RecitifierCom::curr_ack_string);
    return true;
}

bool CommandProcedures::set_current(std::string_view args)
{
    auto fields = CommandsController::args_tokenize(args);
    if (fields.size() < 1)
    {
        CommandsController::send_ans("Must be at least 1 argument!\n");
        return false;
    }
    float current{0.f};
    auto result =
        std::from_chars(fields[0].cbegin(), fields[0].cend(), current);
    if (result.ec != std::errc{})
    {
        CommandsController::send_ans(
            "Wrong first argument - must be real number\n");
        return false;
    }
    auto nonvolatile_flag = std::ranges::find(fields, "non-volatile"sv);

    if (!RecitifierCom::set_current(current, nonvolatile_flag != fields.cend()))
    {
        CommandsController::Error::send_error(
            CommandsController::Error::can_controller_error,
            "Sending CAN frame failed");
        return false;
    }
    RecitifierCom::wait_for_ack = true;
    auto end_time               = HAL_GetTick() + 1000U;
    while (RecitifierCom::wait_for_ack)
    {
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        if (HAL_GetTick() > end_time)
        {
            CommandsController::Error::send_error(
                CommandsController::Error::can_wait_timeout,
                "Timeout elapsed while waiting for ACK");
            RecitifierCom::wait_for_ack = false;
            return false;
        }
    }
    CommandsController::send_ans("Got ack string\n");
    CommandsController::send_ans(RecitifierCom::curr_ack_string);
    return true;
}
